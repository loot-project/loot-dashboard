import './app.scss'
import '../node_modules/bootstrap-icons/font/bootstrap-icons.css'
import App from './App.svelte'
import * as bootstrap from 'bootstrap'


const app = new App({
  target: document.getElementById('app')
})

export default app
