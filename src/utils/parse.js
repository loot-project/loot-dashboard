import { groupBy, groupByItems, convertToCSV } from "../utils/utils";

// Get labels from API entities for an api path
export function getFieldById(data, source, path, property) {
  let display = [];
  if (path) {
    path = Array.isArray(path) ? path : new Array(path);
    path.forEach((p) => {
      const value = data[source].find((i) => i.id == p.split("/").pop());
      display.push(value ? value[property] : "n.c.");
    });
  } else display.push("n.c.");
  return display.join(", ");
}

export function applyFilters(items, filters, period) {
  let filteredItems = [];

  if (items) {
        
    filteredItems = filtersByType(items, filters);
    filteredItems = filterByDate(filteredItems, period);
  }

  return filteredItems;
}

export function filtersByType(items, filters) {
  filters = groupBy(filters, "type");
  let filteredItems = [...items];

  for (const [type, ids] of Object.entries(filters)) {
    if (type == "budgets")
      filteredItems = filteredItems.filter((item) =>
        ids.some((id) => id == item.budget.split("/").pop())
      );
    if (type == "users")
      filteredItems = filteredItems.filter((item) =>
        ids.some((id) => id == item.contributor.split("/").pop())
      );

    if (type == "activities")
      filteredItems = filteredItems.filter((item) => {
        let itemActivities = item.activities.map((i) =>
          parseInt(i.split("/").pop())
        );
        let intersection = itemActivities.filter((i) => ids.includes(i));
        return intersection.length > 0;
      });
  }

  return filteredItems;
}

export function filterByDate(items, period) {
  let filteredItems = [...items];
  if (period.start)
    filteredItems = filteredItems.filter(
      (i) => new Date(i.doned).getTime() >= new Date(period.start).getTime()
    );
  if (period.end)
    filteredItems = filteredItems.filter(
      (i) => new Date(i.doned).getTime() <= new Date(period.end).getTime()
    );
  return filteredItems;
}

export function prepareCSVExport(items) {
  const csv = convertToCSV(items);
  const blob = new Blob([csv], { type: "text/csv;charset=utf-8;" });
  return URL.createObjectURL(blob);
}

export function getItemsGroupedBy(items, type) {
  let groupedItems = [];

  items = groupByItems(items, type);
  for (const [key, value] of Object.entries(items)) {
    groupedItems.push({ type: key, items: value });
  }
  return groupedItems;
}
