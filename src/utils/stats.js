
export function getStatistics(contributions, data = null ) {
    let stat =  contributions.reduce(
        (statObj,value) => {

           
            statObj.totalRetribution += value.retribution ? parseFloat(value.amount) : 0;
            statObj.totalDuration += value.duration ? parseFloat(value.duration) : 0;
            statObj.totalAmountWithDuration += value.duration && value.amount ? parseFloat(value.amount) : 0;
            statObj.totalDurationWithAmount += value.duration && value.amount ? parseFloat(value.duration) : 0;
            
            statObj.contributionWithRetribution += value.retribution ? 1 : 0;
            if( value.amount) {
                statObj.contributionMax = (statObj.contributionMax < value.amount || statObj.contributionMax==null) ? value.amount : statObj.contributionMax;
                statObj.contributionMin = (statObj.contributionMin > value.amount || statObj.contributionMin==null) ? value.amount : statObj.contributionMin;
            }
            
            if(!statObj.totalAmount[value.budget]) statObj.totalAmount[value.budget]=0
            statObj.totalAmount[value.budget] += value.amount ? parseFloat(value.amount) :0;


            return statObj;
        },
        {
            totalAmount : {}, 
            totalDuration : 0, 
            totalRetribution : 0, 
            totalAmountWithDuration : 0, 
            totalDurationWithAmount : 0,
            contributionWithRetribution : 0,
            contributionMax : null,
            contributionMin : null,
        }
    )

    stat.totalRetribution = stat.totalRetribution.toFixed(2);
    stat.totalDuration = stat.totalDuration.toFixed(1);
    stat.totalAmountWithDuration = stat.totalAmountWithDuration.toFixed(2)
    stat.totalDurationWithAmount = stat.totalDurationWithAmount.toFixed(1)

    stat.totalContribution = contributions.length;
    stat.averageContributionAmountByHour = "-";
    if(stat.totalDurationWithAmount>0)
        stat.averageContributionAmountByHour = Math.floor((stat.totalAmountWithDuration / stat.totalDurationWithAmount))
    stat.retributionAmountRatio = Math.floor((stat.totalRetribution / stat.totalAmount) * 100)
    stat.retributedContributionRatio = Math.floor(stat.contributionWithRetribution * 100 / stat.totalContribution)
    
    const distinctBudgets = [...new Set(contributions.map((item) => item.budget))];
    stat.numberOfBudgets = distinctBudgets.length;

    return stat
}