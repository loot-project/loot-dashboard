import { DateTime, Interval, Duration } from "luxon";

function getTimeFrame(_year) {
  let startDate = DateTime.local(parseInt(_year), 1, 1);
  let duration = Duration.fromObject({ years: 1 });
  return Interval.after(startDate, duration);
}

export function getGraphRange(items,_selected) {
  
  let selected = null;
  let years = items.map( i => new Date(i.doned).getFullYear()) ;
  let uniqueYears = [...new Set(years)];
  
  (_selected !=null && uniqueYears.includes(_selected)) ? selected = _selected : selected = uniqueYears[uniqueYears.length-1];

  let range = {
    years : uniqueYears,
    selected : selected
  };
  return range;
}

export const accumulate = (arr) =>
  arr.map(
    (
      (sum) => (value) =>
        (sum += value)
    )(0)
  );

export function getGraphLabels(_year) {
  let _timeFrame = getTimeFrame(_year);
  let graphLabels = [];
  if (_timeFrame) {
    let cursor = _timeFrame.start.startOf("week");
    while (cursor < _timeFrame.end) {
      graphLabels.push(cursor.weekNumber);
      cursor = cursor.plus({ weeks: 1 });
    }
  }
  return graphLabels;
}

export function getGraphValues(items, _year) {
  let _timeFrame = getTimeFrame(_year);
  let monthlyData = [];
  if (items.length) {
    let cursor = _timeFrame.start.startOf("week");
    while (cursor < _timeFrame.end) {
      const result = items.filter((i) => {
        let itemDate = DateTime.fromISO(i.doned);
        return itemDate >= cursor && itemDate < cursor.plus({ weeks: 1 });
      });
      monthlyData.push(result);
      cursor = cursor.plus({ weeks: 1 });
    }
  }
  return monthlyData;
}
