export function getFirstAndLastDayOfWeek(d) {
  const date = new Date(d);
  const day = date.getDay(); // 👉️ get day of week
  const diff = date.getDate() - day + (day === 0 ? -6 : 1);
  const firstDay = new Date(date.setDate(diff))
  const lastDay = new Date(firstDay);
  lastDay.setDate(lastDay.getDate() + 6);
  return {firstDay,lastDay};
}

export function getFirstAndLastDayOfMonth(d) {
  const date = new Date(d);
  const firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
  const lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
  return {firstDay,lastDay};
}

export function subtractWeeks(numOfWeeks, date = new Date()) {
  const dateCopy = new Date(date.getTime());
  dateCopy.setDate(dateCopy.getDate() - numOfWeeks * 7);
  return dateCopy;
}

export function getWeeks(number){
  let weeks = []
  let currentDate = new Date();
  for(let i=0; i<number; i++){
    let lastAndFirstDaysOfTheWeek = getFirstAndLastDayOfWeek(currentDate);
    weeks.push(lastAndFirstDaysOfTheWeek) 
    currentDate = subtractWeeks(1,currentDate)
  }
  return weeks;

}
