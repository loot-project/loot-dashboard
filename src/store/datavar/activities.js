export const activities = [
  {
    "id": 15,
    "title": "communication",
    "budgets": ["/api/budgets/35", "/api/budgets/37", "/api/budgets/40"],
    "contributions": ["/api/contributions/32", "/api/contributions/36"]
  },
  {
    "id": 16,
    "title": "coordination",
    "budgets": ["/api/budgets/36", "/api/budgets/44"],
    "contributions": [
      "/api/contributions/28",
      "/api/contributions/29",
      "/api/contributions/34"
    ]
  },
  {
    "id": 17,
    "title": "administratif",
    "budgets": ["/api/budgets/38", "/api/budgets/39"],
    "contributions": [
      "/api/contributions/29",
      "/api/contributions/31",
      "/api/contributions/32"
    ]
  }
]
