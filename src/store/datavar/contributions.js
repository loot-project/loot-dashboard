export const contributions = [
  {
    "id": 26,
    "contributor": "/api/users/68",
    "budget": "/api/budgets/34",
    "duration": 1.5,
    "description": "user1 contribution",
    "amount": 10,
    "created": "2022-10-21T08:59:42+00:00",
    "doned": "2022-09-30T00:00:00+00:00",
    "retribution": null,
    "activities": []
  },
  {
    "id": 27,
    "contributor": "/api/users/71",
    "budget": "/api/budgets/39",
    "duration": 10,
    "description": "Quos culpa ut ab voluptas sed a.",
    "amount": 2008,
    "created": "2022-10-21T08:59:42+00:00",
    "doned": "2022-09-24T00:00:00+00:00",
    "retribution": null,
    "activities": []
  },
  {
    "id": 28,
    "contributor": "/api/users/71",
    "budget": "/api/budgets/42",
    "duration": 1,
    "description": "Officia aut aut blanditiis et ducimus eos odit amet.",
    "amount": 1874,
    "created": "2022-10-21T08:59:42+00:00",
    "doned": "2022-08-03T00:00:00+00:00",
    "retribution": "/api/retributions/8",
    "activities": ["/api/activities/16"]
  },
  {
    "id": 29,
    "contributor": "/api/users/70",
    "budget": "/api/budgets/43",
    "duration": 4,
    "description": "Quidem ut sunt et quidem est accusamus aut nemo.",
    "amount": 1890,
    "created": "2022-10-21T08:59:42+00:00",
    "doned": "2022-08-11T00:00:00+00:00",
    "retribution": null,
    "activities": ["/api/activities/16", "/api/activities/17"]
  },
  {
    "id": 30,
    "contributor": "/api/users/70",
    "budget": "/api/budgets/41",
    "duration": 4,
    "description": "Enim ex eveniet facere sunt.",
    "amount": 407,
    "created": "2022-10-21T08:59:42+00:00",
    "doned": "2022-10-14T00:00:00+00:00",
    "retribution": null,
    "activities": []
  },
  {
    "id": 31,
    "contributor": "/api/users/70",
    "budget": "/api/budgets/41",
    "duration": 5,
    "description": "Architecto fugit repellendus illo veritatis qui.",
    "amount": 1098,
    "created": "2022-10-21T08:59:42+00:00",
    "doned": "2022-06-26T00:00:00+00:00",
    "retribution": null,
    "activities": ["/api/activities/17"]
  },
  {
    "id": 32,
    "contributor": "/api/users/71",
    "budget": "/api/budgets/39",
    "duration": 4,
    "description": "Omnis aut incidunt sunt cumque asperiores incidunt iure.",
    "amount": 501,
    "created": "2022-10-21T08:59:42+00:00",
    "doned": "2022-08-17T00:00:00+00:00",
    "retribution": null,
    "activities": ["/api/activities/15", "/api/activities/17"]
  },
  {
    "id": 33,
    "contributor": "/api/users/71",
    "budget": "/api/budgets/39",
    "duration": 1,
    "description": "Exercitationem est rem dicta voluptas fuga totam reiciendis.",
    "amount": 2197,
    "created": "2022-10-21T08:59:42+00:00",
    "doned": "2022-04-19T00:00:00+00:00",
    "retribution": null,
    "activities": []
  },
  {
    "id": 34,
    "contributor": "/api/users/70",
    "budget": "/api/budgets/38",
    "duration": 6,
    "description": "Recusandae qui cupiditate eos.",
    "amount": 2241,
    "created": "2022-10-21T08:59:42+00:00",
    "doned": "2022-04-17T00:00:00+00:00",
    "retribution": null,
    "activities": ["/api/activities/16"]
  },
  {
    "id": 35,
    "contributor": "/api/users/70",
    "budget": "/api/budgets/41",
    "duration": 6,
    "description": "Incidunt magnam molestias et quibusdam et ab quo voluptatum.",
    "amount": 416,
    "created": "2022-10-21T08:59:42+00:00",
    "doned": "2021-05-08T00:00:00+00:00",
    "retribution": null,
    "activities": []
  },
  {
    "id": 36,
    "contributor": "/api/users/70",
    "budget": "/api/budgets/41",
    "duration": 1,
    "description": "Aut atque possimus aut dolores quis totam incidunt.",
    "amount": 1376,
    "created": "2022-10-21T08:59:42+00:00",
    "doned": "2022-04-06T00:00:00+00:00",
    "retribution": null,
    "activities": ["/api/activities/15"]
  },
  {
    "id": 37,
    "contributor": "/api/users/70",
    "budget": "/api/budgets/41",
    "duration": 4,
    "description": "Nouvelle contrib la meme semaine.",
    "amount": 1376,
    "created": "2022-10-21T08:59:42+00:00",
    "doned": "2022-04-07T00:00:00+00:00",
    "retribution": null,
    "activities": ["/api/activities/15"]
  }
]