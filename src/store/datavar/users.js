export const users = [
  {
    "id": 68,
    "email": "user1@localhost.com",
    "username": "user1@localhost.com",
    "roles": ["ROLE_USER"],
    "firstname": "User",
    "lastname": "NAME_1",
    "name": "User NAME_1",
    "budgets": ["/api/budgets/34"],
    "retributions": []
  },
  {
    "id": 69,
    "email": "user2@localhost.com",
    "username": "user2@localhost.com",
    "roles": ["ROLE_USER"],
    "firstname": "User",
    "lastname": "NAME_2",
    "name": "User NAME_2",
    "budgets": [],
    "retributions": []
  },
  {
    "id": 70,
    "email": "api_1@localhost.com",
    "username": "api_1@localhost.com",
    "roles": ["ROLE_USER"],
    "firstname": "Adah",
    "lastname": "Reichel",
    "name": "Adah REICHEL",
    "budgets": [
      "/api/budgets/36",
      "/api/budgets/37",
      "/api/budgets/38",
      "/api/budgets/41",
      "/api/budgets/43",
      "/api/budgets/44"
    ],
    "retributions": []
  },
  {
    "id": 71,
    "email": "api_2@localhost.com",
    "username": "api_2@localhost.com",
    "roles": ["ROLE_USER"],
    "firstname": "Zetta",
    "lastname": "Stroman",
    "name": "Zetta STROMAN",
    "budgets": [
      "/api/budgets/35",
      "/api/budgets/36",
      "/api/budgets/38",
      "/api/budgets/39",
      "/api/budgets/40",
      "/api/budgets/41",
      "/api/budgets/42",
      "/api/budgets/43"
    ],
    "retributions": ["/api/retributions/8"]
  }
]
