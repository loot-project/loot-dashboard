import { readable,derived } from "svelte/store";
import {navigation} from "../store/navigation"
import { filters } from "./filters";
import { dates } from "./dates";
import { applyFilters } from "../utils/parse"

// import {activities} from './datavar/activities';
// import {contributions} from './datavar/contributions';
// import {retributions} from './datavar/retributions';
// import {budgets} from './datavar/budgets';
// import {users} from './datavar/users';

// export const data = readable({contributions:[],budgets:[],users:[],activities:[]}, function start(set) {

//     const data = {}
//     data.contributions = contributions;
//     data.budgets = budgets;
//     data.users = users;
//     data.activities = activities;
//     data.retributions = retributions;
//     set (data)
// })

export const data = readable({contributions:[],budgets:[],users:[],activities:[]}, function start(set) {

    navigation.isLoading(true);

    const dataPath = import.meta.env.VITE_DATA_PATH;

    const cPromise = fetch(`${dataPath}/contributions.json`);
    const bPromise = fetch(`${dataPath}/budgets.json`);
    const uPromise = fetch(`${dataPath}/users.json`);
    const aPromise = fetch(`${dataPath}/activities.json`);
    const rPromise = fetch(`${dataPath}/retributions.json`);
    Promise.all([cPromise,bPromise,uPromise,aPromise,rPromise]).then(results => {
        const cValue = results[0].json();
        const bValue = results[1].json();
        const uValue = results[2].json();
        const aValue = results[3].json();
        const rValue = results[4].json();
        Promise.all([cValue,bValue,uValue,aValue,rValue]).then(values => {
            const data = {}
            data.contributions = values[0];
            data.budgets = values[1];
            data.users = values[2];
            data.activities = values[3]
            data.retributions = values[4]
            set (data)
            navigation.isLoading(false);
        });     
        

    }).catch(reason => {
        console.log(`Error while getting data : ${reason}`)
        navigation.isLoading(false);
    });
})

/*async function getData(type,offset){
    const dataPath = import.meta.env.VITE_DATA_PATH;
    console.log(dataPath);
    const url = dataPath+'/'+type+'.json?page='+offset;
    console.log(url)
    const response = await fetch(url);
    const data = await response.json();
    return data;
}

async function getPagedData(type){
    let data = []
    let offset = 1
    let keep = true;
    while (keep) {
        const res = await getData(type,offset);
        data = [...res, ...data]
        res.length>0 ? offset++ : keep = false
    }
    return data
}

export const data = readable({contributions:[],budgets:[],users:[],activities:[]}, function start(set) {

    navigation.isLoading(true);

    const c = getPagedData('contributions');
    const b = getPagedData('budgets');
    const u = getPagedData('users');
    const a = getPagedData('activities');
    const r = getPagedData('retributions');

    Promise.all([c,b,u,a,r]).then ( res => {
        const data =  {};
        data.contributions = res[0];
        data.budgets = res[1];
        data.users = res[2];
        data.activities = res[3];
        data.retributions = res[4];
        set(data)
        navigation.isLoading(false);
    }).catch(reason => {
        console.log(`Error while getting data : ${reason}`)
        navigation.isLoading(false);
    });
     
     
     


         
     
})
*/

export const filteredData = derived(
	[data,filters,dates],
	([$data,$filters,$dates]) => applyFilters($data.contributions, $filters, $dates)
);


