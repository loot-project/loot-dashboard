import { writable } from "svelte/store";

function createDatasource (){
    const {subscribe, set, update} = writable( {type : 'local',url : ''} )

    return {
        subscribe,
        setUrl : (url) => update ( n=> {
            n.url = url
            return n
        }),
        setType : (type) => update ( n=> {
            n.type = type
            return n
        })
    }
}

export const datasource = createDatasource()