import { writable } from 'svelte/store';

function createDates() {
	const { subscribe, set, update } = writable({start:'',end:''});

	return {
		subscribe,
        setStartDate  : (d) => update( n=> {
            n.start = d;
            return n;
        }),
        setEndDate  : (d) => update( n=> {
            n.end = d
            return n;
        })
	};
}

export const dates = createDates();