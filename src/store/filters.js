import { writable } from 'svelte/store';

function createFilters() {
	const { subscribe, set, update } = writable([]);

	return {
		subscribe,
        toggleFilter : (obj) => update( n => {
            let itemIndex = n.findIndex( i => i.item.id === obj.item.id)
            if (itemIndex > -1) {
                n.splice(itemIndex,1)
            }else {
                n.push(obj)
            }
            return n;
        })
	};
}

export const filters = createFilters();