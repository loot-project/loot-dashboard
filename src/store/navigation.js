import { writable } from "svelte/store";

function createNavigation (){
    const {subscribe, set, update} = writable( {view : 'home', loading : false} )

    return {
        subscribe,
        setView : (view) => update ( n=> {
            n.view = view
            return n
        }), 
        isLoading : (loading) => update ( n => {
            n.loading = loading
            return n
        })
    }
}

export const navigation = createNavigation()