# Loot Dashboard

This demo app shows how to get and display data from the [Loot app](https://gitlab.com/loot-project/loot-app)

## Loot API

[An read-only API is under development. For the moment, it exposes each entity as an endpoint. For more documentation on the API, go to 
<https://your-installation-url>/api/

## The dahsboard

As Loot exposes all the data it contains, everyone can build the dashbaord that will match its needs.
The current dahsboard gives a quick and straight forward example of what can be done using simple technology : 

[Svelte](https://svelte.dev) - Javascript framework
[Bootstrap](https://getbootstrap.com) - CSS framework

## Runing the app

### Production with Gitlab Pages

Fork the the repository and setup the variable LOOT_URI in the CI/CD settings of the forking project. For example : `https://loot.yourdomain.com/api`
Run the pipeline to generate a new version of your dashboard :

- Go to https://gitlab.com/your-group/loot-dashboard/-/pipelines
- Click "Run the pipeline"

Now you can browse the dashboard of your loot instance : https://your-group.gitlab.io/loot-dashboard/

### Development

Clone this repo and :
- npm install
- npm run dev
- go to http://localhost:5173

## Configuration

### Defining your source

There's a .env file as the project root.
Configure the VITE_DATA_PATH environnement variable to work with your loot installation api :
- VITE_DATA_PATH=http://loot.local/api

Or use the samples locals files within this repo : 
- VITE_DATA_PATH=/src/store/data/

