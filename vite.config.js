import { defineConfig } from 'vite'
import { svelte } from '@sveltejs/vite-plugin-svelte'
import sveltePreprocess from 'svelte-preprocess';


// https://vitejs.dev/config/
export default defineConfig({
  base: '',
  publicDir : './assets',
  build: { 
    outDir: './public'    
  },
  plugins: [
    svelte({
      preprocess: sveltePreprocess({
        preserve: ['ld+json'],
        scss: {
          importer: [
            (url) => {
              // Redirects tilde-prefixed imports to node_modules
              if (/^~/.test(url))
                return { file: `../node_modules/${url.replace('~', '')}` };
              return null;
            },
          ],
          quietDeps: true,
        },
      }),
    }),
  ]
})
